var express = require('express');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);

app.use(express.static('assets'));
app.get('/', function(req, res){
	res.sendFile(__dirname + '/index.html');
});

var songs = [];

io.on('connection', function(socket){
	console.log('a user connected');
	var user = {
        id: socket.id,
        room: 'room1'
    };

	socket.join(user.room);
	socket.emit('songs', songs);

	socket.on('addSong', function(song) {
		songs.push(song);
		io.to(user.room).emit('songs', songs);
	});
});

http.listen(3000, function(){
	console.log('server is at *:3000');
});
